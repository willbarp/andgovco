import { Component, OnInit } from '@angular/core';
import{TramitesService} from '../tramites.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css']
})
export class TramitesComponent implements OnInit {
 
  tramite:any[]=[];
  tramitebd:any;
  
  constructor(private _servicio:TramitesService) 
  {
    this.tramite = _servicio.Obtenertramite();
    _servicio.Obtenertramitebd().subscribe(item => {this.tramitebd=item})

    console.log("tramite");
    console.log(this.tramite);
    console.log("tramitebd");
    console.log(this.tramitebd);
  }

  ngOnInit(): void {
  }

}


