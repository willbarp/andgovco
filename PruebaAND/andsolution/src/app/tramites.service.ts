import { Injectable } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from '../environments/environment';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';



export interface Item{nombre:string}
// Servicios

@Injectable({
  providedIn: 'root'
})


export class TramitesService {

  tramite:any[]=[
    { 
      Nombretramite :' Certificado de antecedentes de responsabilidad fiscal',
      descripcion:'************************'
    },
    { 
      Nombretramite :'Certificado Antecedentes Disciplinarios Procuraduría',
      descripcion:'************************'
    },
    { 
      Nombretramite :'Certificado de Antecedentes Judiciales policía',
      descripcion:'************************'
    },
        
  ]
  private itemstramites: AngularFirestoreCollection<Item>;
  item :Observable<Item[]>;
  
  constructor(private db: AngularFirestore) {
    console.log('tramite funcionando');
    
    this.itemstramites = db.collection<Item>('itemstramites');
    this.item= this.itemstramites.valueChanges();
    console.log(this.item);
   }

   Obtenertramite()
   {

      return this.tramite; 
   }
   Obtenertramitebd()
   {

      return this.item; 
   }
   
}