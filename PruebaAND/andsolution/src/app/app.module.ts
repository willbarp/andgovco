import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { PiedePaginaComponent } from './piede-pagina/piede-pagina.component';
import { TramitesComponent } from './tramites/tramites.component';
import { InformateComponent } from './informate/informate.component';
import { InteresComponent } from './interes/interes.component';
import { OpinionComponent } from './opinion/opinion.component';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from '../environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

// Servicios
import{ TramitesService } from './tramites.service';

const routes: Routes = [
  { path: 'Tramites', component: TramitesComponent },
  { path: 'Opinion', component: OpinionComponent },
  { path: 'Informate', component: InformateComponent },
  { path: 'Interes', component: InteresComponent },
  
  { path: '', redirectTo: '/Tramites', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    PiedePaginaComponent,
    TramitesComponent,
    InformateComponent,
    InteresComponent,
    OpinionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
