import { Component, OnInit } from '@angular/core';
import{TramitesService} from '../tramites.service';
@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css']
})
export class TramitesComponent implements OnInit {
 
  tramite:any[]=[];
  constructor(private _servicio:TramitesService) 
  {
    this.tramite=_servicio.Obtenertramite();
  }

  ngOnInit(): void {
  }

}


