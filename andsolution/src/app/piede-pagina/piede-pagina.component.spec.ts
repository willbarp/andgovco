import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PiedePaginaComponent } from './piede-pagina.component';

describe('PiedePaginaComponent', () => {
  let component: PiedePaginaComponent;
  let fixture: ComponentFixture<PiedePaginaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PiedePaginaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PiedePaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
